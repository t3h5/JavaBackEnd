package com.t3h.springmvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
    @RequestMapping(value = "/hello")
    public String hello(ModelMap map) {
        map.addAttribute("msg", "Hello World.");
        return "HelloView";
    }
}
