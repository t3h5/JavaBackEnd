package com.t3h.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AppException extends RuntimeException {
    private int code;

    public AppException(int code, String message) {
        super(message);
        this.code = code;
    }
}
