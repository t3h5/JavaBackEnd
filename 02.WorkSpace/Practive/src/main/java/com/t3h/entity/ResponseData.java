package com.t3h.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseData {
    private String type;
    private Object data;
    private Object error;

    public ResponseData() {

    }

    public ResponseData(String type, Object o) {
        this.type = type;
        if (o instanceof Exception)
            this.error = o;
        else
            this.data = o;
    }
}
