package com.t3h.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Customer {
    private int cusNumber;
    private String cusName;
    private String contactLastName;
    private String contactFirstName;
    private String phone;
    private String addrLine1;
    private String addrLine2;
    private String city;
    private String state;
    private String postalCode;
    private String country;
    private Employee salesRepEmployee;
    private String creditLimit;
}
