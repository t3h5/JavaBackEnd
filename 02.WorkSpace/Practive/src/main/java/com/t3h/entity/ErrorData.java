package com.t3h.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class ErrorData {
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="E, dd MMM yyyy HH:mm:ss z", timezone="GMT")
    private Date timeStamp;
    private int code;
    private Object error;

    public ErrorData() {
        timeStamp = new Date();
    }

    public ErrorData(int code, Object error) {
        this();
        this.code = code;
        this.error = error;
    }
}
