package com.t3h.service.impl;

import com.t3h.entity.AppException;
import com.t3h.entity.Customer;
import com.t3h.repository.impl.CustomerRepo;
import com.t3h.service.AppService;
import com.t3h.utils.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class CustomerService implements AppService {
    @Autowired
    private CustomerRepo repo;

    @Override
    public List<?> getAllResource() throws SQLException {
        return repo.getAll();
    }

    @Override
    public Object getResourceById(int id) throws SQLException {
        return repo.getById(id);
    }

    @Override
    public int addResource(Object o) throws SQLException {
        return repo.add(o);
    }

    @Override
    public int updateResource(int id, Object o) throws SQLException {
        return repo.update(id, o);
    }

    @Override
    public int deleteResourceById(int id) throws SQLException {
        return repo.deleteById(id);
    }

    @Override
    public int deleteAllResource() throws SQLException {
        return repo.delete();
    }

    public void validateData(Customer newCus) {
        if (newCus == null)
            throw new AppException(500, MessageProperties.getData("msg001"));
        System.out.println(newCus.getCreditLimit());
        if (newCus.getCusName() == null || newCus.getCusName().isEmpty())
            throw new AppException(500, MessageProperties.getData("msg002"));
        if (newCus.getContactLastName() == null || newCus.getContactLastName().isEmpty())
            throw new AppException(500, MessageProperties.getData("msg003"));
        if (newCus.getContactFirstName() == null || newCus.getContactFirstName().isEmpty())
            throw new AppException(500, MessageProperties.getData("msg004"));
        if (newCus.getPhone() == null || newCus.getPhone().isEmpty())
            throw new AppException(500, MessageProperties.getData("msg005"));
        if (newCus.getAddrLine1() == null || newCus.getAddrLine1().isEmpty())
            throw new AppException(500, MessageProperties.getData("msg006"));
        if (newCus.getCity() == null || newCus.getCity().isEmpty())
            throw new AppException(500, MessageProperties.getData("msg007"));
        if (newCus.getCountry() == null || newCus.getCountry().isEmpty())
            throw new AppException(500, MessageProperties.getData("msg008"));
    }
}
