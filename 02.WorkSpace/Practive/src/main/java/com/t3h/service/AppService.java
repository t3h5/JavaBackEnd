package com.t3h.service;

import com.t3h.entity.Customer;

import java.sql.SQLException;
import java.util.List;

public interface AppService {
    public List<?> getAllResource() throws SQLException;

    public Object getResourceById(int id) throws SQLException;

    public int addResource(Object o) throws SQLException;

    public int updateResource(int id, Object o) throws SQLException;

    public int deleteResourceById(int id) throws SQLException;

    public int deleteAllResource() throws SQLException;
}
