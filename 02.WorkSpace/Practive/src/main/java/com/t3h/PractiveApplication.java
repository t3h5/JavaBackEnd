package com.t3h;

import com.t3h.utils.ConnectDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
public class PractiveApplication {

    public static void main(String[] args) {

        SpringApplication.run(PractiveApplication.class, args);

    }
}
