package com.t3h.repository;

import java.sql.SQLException;
import java.util.List;

public interface DatabaseRepo {
    public List<?> getAll() throws SQLException;

    public Object getById(int id) throws SQLException;

    public int add(Object o) throws SQLException;

    public int update(int id, Object o) throws SQLException;

    public int deleteById(int id) throws SQLException;

    public int delete() throws SQLException;

}
