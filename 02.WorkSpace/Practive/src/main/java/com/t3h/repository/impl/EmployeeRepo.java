package com.t3h.repository.impl;

import com.t3h.entity.Employee;
import com.t3h.repository.DatabaseRepo;
import com.t3h.utils.ConnectDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EmployeeRepo implements DatabaseRepo {
    @Autowired
    private ConnectDB connectDB;

    public final Map<Integer, Employee> employees = new LinkedHashMap<>();

    @Override
    public List<?> getAll() throws SQLException {
        try (Connection con = connectDB.getConnection();
             Statement st = con.createStatement()) {
            String sql = "select * from employees";
            st.setFetchSize(1000);
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Employee emp = new Employee();
                    int id = rs.getInt("employeeNumber");
                    emp.setEmployeeNum(id);
                    emp.setLastName(rs.getString("lastName"));
                    emp.setFirstName(rs.getString("firstName"));
                    emp.setExtension(rs.getString("extension"));
                    emp.setEmail(rs.getString("email"));
                    emp.setOfficeCode(rs.getString("officeCode"));
                    emp.setReportsTo(rs.getInt("reportsTo"));
                    emp.setJobTitle(rs.getString("jobTitle"));

                    synchronized (this) {
                        if (!this.employees.containsKey(id)) {
                            this.employees.put(id, emp);
                        }
                    }
                }
                return new ArrayList<>(this.employees.values());
            }
        }
    }

    @Override
    public Object getById(int id) throws SQLException {
        if (this.employees.containsKey(id)) {
            return this.employees.get(id);
        }
        this.getAll();
        return this.employees.get(id);
    }

    @Override
    public int add(Object o) throws SQLException {
        return 0;
    }

    @Override
    public int update(int id, Object o) throws SQLException {
        return 0;
    }

    @Override
    public int deleteById(int id) throws SQLException {
        return 0;
    }

    @Override
    public int delete() throws SQLException {
        return 0;
    }
}
