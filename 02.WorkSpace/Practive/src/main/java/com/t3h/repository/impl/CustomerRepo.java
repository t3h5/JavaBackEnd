package com.t3h.repository.impl;

import com.t3h.entity.AppException;
import com.t3h.entity.Customer;
import com.t3h.entity.Employee;
import com.t3h.repository.DatabaseRepo;
import com.t3h.utils.ConnectDB;
import com.t3h.utils.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

@Repository
public class CustomerRepo implements DatabaseRepo {
    @Autowired
    private ConnectDB connectDB;

    @Autowired
    private EmployeeRepo employeeRepo;

    public final Map<Integer, Customer> customers = new LinkedHashMap<>();

    @Override
    public List<?> getAll() throws SQLException {
        try (Connection con = connectDB.getConnection();
             Statement st = con.createStatement()) {
            String sql = "select * from customers";
            st.setFetchSize(1000);
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    Customer cus = new Customer();
                    int id = rs.getInt("customerNumber");
                    cus.setCusNumber(id);
                    cus.setCusName(rs.getString("customerName"));
                    cus.setContactLastName(rs.getString("contactLastName"));
                    cus.setAddrLine1(rs.getString("addressLine1"));
                    cus.setAddrLine2(rs.getString("addressLine2"));
                    cus.setCity(rs.getString("city"));
                    cus.setState(rs.getString("state"));
                    cus.setPostalCode(rs.getString("postalCode"));
                    cus.setCountry(rs.getString("country"));

                    int saleRepEmployeeNum = rs.getInt("salesRepEmployeeNumber");
                    cus.setSalesRepEmployee((Employee) employeeRepo.getById(saleRepEmployeeNum));
                    cus.setCreditLimit(rs.getString("creditLimit"));

                    synchronized (this) {
                        if (!this.customers.containsKey(id)) {
                            this.customers.put(id, cus);
                        }
                    }
                }
                return new ArrayList<>(this.customers.values());
            }
        }
    }

    @Override
    public Object getById(int id) throws SQLException {
        if (this.customers.containsKey(id)) {
            return this.customers.get(id);
        }
        this.getAll();
        return this.customers.get(id);
    }

    @Override
    public int add(Object o) throws SQLException {
        if (!(o instanceof Customer cus)) {
            return 0;
        }
        try (Connection con = connectDB.getConnection()) {
            String sql = new StringBuilder()
                    .append("insert into customers(customerNumber,customerName,contactLastName,")
                    .append("contactFirstName,phone,addressLine1,addressLine2,city,state,")
                    .append("postalCode,country,salesRepEmployeeNumber,creditLimit) values")
                    .append("(?,?,?,?,?,?,?,?,?,?,?,?,?)").toString();
            String sql2 = "select max(customerNumber) from customers";
            try (PreparedStatement pst = con.prepareStatement(sql);
                 Statement st = con.createStatement();
                 ResultSet rs2 = st.executeQuery(sql2)) {

                rs2.next();
                int id = rs2.getInt(1) + 1;
                int index = 1;
                pst.setInt(index++, id);
                pst.setString(index++, cus.getCusName());
                pst.setString(index++, cus.getContactLastName());
                pst.setString(index++, cus.getContactFirstName());
                pst.setString(index++, cus.getPhone());
                pst.setString(index++, cus.getAddrLine1());
                pst.setString(index++, cus.getAddrLine2());
                pst.setString(index++, cus.getCity());
                pst.setString(index++, cus.getState());
                pst.setString(index++, cus.getPostalCode());
                pst.setString(index++, cus.getCountry());
                pst.setInt(index++, cus.getSalesRepEmployee().getEmployeeNum());
                pst.setDouble(index, Double.parseDouble(cus.getCreditLimit()));

                int result = pst.executeUpdate();
                if (result > 0) {
                    con.commit();
                    synchronized (this) {
                        this.customers.put(id, (Customer) this.getById(id));
                    }

                }
                return result;
            }
        }
    }

    @Override
    public int update(int id, Object o) throws SQLException {
        if (!(o instanceof Customer cus)) {
            return 0;
        }
        try (Connection con = connectDB.getConnection()) {
            List<String> args = new LinkedList<>();
            Customer cus2 = this.customers.get(id);
            if (cus2 == null) {
                this.getAll();
                cus2 = this.customers.get(id);
            }

            StringBuilder sql = new StringBuilder()
                    .append("update customers set ");
            int i = 0;
            if (cus.getCusName() != null) {
                sql.append(" customerName = ?,");
                args.add(cus.getCusName());
                cus2.setCusName(cus.getCusName());
                i++;
            }
            if (cus.getContactLastName() != null) {
                sql.append(" contactLastName = ?,");
                args.add(cus.getContactLastName());
                cus2.setContactLastName(cus.getContactLastName());
                i++;
            }
            if (cus.getContactFirstName() != null) {
                sql.append(" contactFirstName = ?,");
                args.add(cus.getContactFirstName());
                cus2.setContactFirstName(cus.getContactFirstName());
                i++;
            }
            if (cus.getPhone() != null) {
                sql.append(" phone = ?,").append(cus.getPhone()).append(", ");
                args.add(cus.getPhone());
                cus2.setPhone(cus.getPhone());
                i++;
            }
            if (cus.getAddrLine1() != null) {
                sql.append(" addressLine1 = ?,");
                args.add(cus.getAddrLine1());
                cus2.setAddrLine1(cus.getAddrLine1());
                i++;
            }
            if (cus.getAddrLine2() != null) {
                sql.append(" addressLine2 = ?,");
                args.add(cus.getAddrLine2());
                cus2.setAddrLine2(cus.getAddrLine2());
                i++;
            }
            if (cus.getCity() != null) {
                sql.append(" city = ?,");
                args.add(cus.getCity());
                cus2.setCity(cus.getCity());
                i++;
            }
            if (cus.getState() != null) {
                sql.append(" state = ?,");
                args.add(cus.getState());
                cus2.setState(cus.getState());
                i++;
            }
            if (cus.getPostalCode() != null) {
                sql.append(" postalCode = ?,");
                args.add(cus.getPostalCode());
                cus2.setPostalCode(cus.getPostalCode());
                i++;
            }
            if (cus.getCountry() != null) {
                sql.append(" country = ?,");
                args.add(cus.getCountry());
                cus2.setCountry(cus.getCountry());
                i++;
            }
            if (cus.getSalesRepEmployee() != null
                    && cus.getSalesRepEmployee().getEmployeeNum() != 0) {
                sql.append(" salesRepEmployeeNumber = ?,");
                args.add(String.valueOf(cus.getSalesRepEmployee().getEmployeeNum()));
                cus2.setSalesRepEmployee(cus.getSalesRepEmployee());
                i++;
            }
            if (cus.getCreditLimit() != null) {
                sql.append(" creditLimit = ?,");
                args.add(cus.getCreditLimit());
                cus2.setCreditLimit(cus.getCreditLimit());
                i++;
            }
            if (i == 0)
                throw new AppException(500, MessageProperties.getData("msg009"));

            sql.deleteCharAt(sql.length() - 1);
            sql.append(" where customerNumber = ?");

            System.out.println(sql);
            try (PreparedStatement pst = con.prepareStatement(sql.toString())) {
                int index = 1;
                for (String arg : args) {
                    pst.setNString(index++, arg);
                }
                pst.setInt(index, id);

                int result = pst.executeUpdate();
                if (result > 0) {
                    cus.setCusNumber(id);
                    synchronized (this) {
                        this.customers.put(id, cus2);
                    }
                    con.commit();
                }
                return result;
            }
        }
    }

    @Override
    public int deleteById(int id) throws SQLException {
        try (Connection con = connectDB.getConnection()) {
            String sql = "delete from customers where customerNumber = ?";
            try (PreparedStatement pst = con.prepareStatement(sql)) {
                pst.setInt(1, id);
                int result = pst.executeUpdate();
                if (result > 0) {
                    synchronized (this) {
                        this.customers.remove(id);
                    }
                    con.commit();
                }
                return result;
            }
        }
    }

    @Override
    public int delete() throws SQLException {
        try (Connection con = connectDB.getConnection();
             Statement st = con.createStatement()) {
            String sql = "delete from customers ";

            int result = st.executeUpdate(sql);
            if (result > 0) {
                synchronized (this) {
                    this.customers.clear();
                }
                con.commit();
            }
            return result;
        }
    }
}
