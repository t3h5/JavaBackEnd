package com.t3h.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.t3h.entity.Customer;
import com.t3h.entity.ResponseData;
import com.t3h.service.impl.CustomerService;
import com.t3h.utils.ApiUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@Controller
@RequestMapping(value = "/api/V1")
public class CustomerController {

    @Autowired
    private CustomerService service;

    @RequestMapping(value = {"/customers"},
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> getAll() throws SQLException, JsonProcessingException {
        ResponseData responseData = new ResponseData("SUCCESS", service.getAllResource());
        return ApiUtil.response(null, responseData, HttpStatus.OK);
    }

    @RequestMapping(value = "/customers/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> getById(@PathVariable(name = "id") int id)
            throws SQLException, JsonProcessingException {
        ResponseData responseData = new ResponseData("SUCCESS", service.getResourceById(id));
        return ApiUtil.response(null, responseData, HttpStatus.OK);
    }

    @RequestMapping(value = "/customers",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> add(@RequestBody Customer newCus)
            throws SQLException, JsonProcessingException {
        // validate
        service.validateData(newCus);

        ResponseData responseData = new ResponseData("SUCCESS", service.addResource(newCus));
        return ApiUtil.response(null, responseData, HttpStatus.OK);
    }


    @RequestMapping(value = "/customers/{id}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(@RequestBody Customer newCus,
                                    @PathVariable(name = "id") int id)
            throws SQLException, JsonProcessingException {

        ResponseData responseData = new ResponseData("SUCCESS", service.updateResource(id, newCus));
        return ApiUtil.response(null, responseData, HttpStatus.OK);
    }

    @RequestMapping(value = "customers/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteById(@PathVariable(name = "id") int id)
            throws SQLException, JsonProcessingException {
        ResponseData responseData = new ResponseData("SUCCESS", service.deleteResourceById(id));
        return ApiUtil.response(null, responseData, HttpStatus.OK);
    }

}
