package com.t3h.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.t3h.entity.AppException;
import com.t3h.entity.ErrorData;
import com.t3h.entity.ResponseData;
import com.t3h.utils.ApiUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class ExceptionController {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<?> handleException(Exception e) throws JsonProcessingException {
        ErrorData errorData;
        ResponseData response = new ResponseData();
        response.setType("ERROR");
        if (e instanceof AppException appException)
            response.setError(new ErrorData(appException.getCode(), appException.getMessage()));
        else if (e instanceof NoHandlerFoundException)
            response.setError(new ErrorData(400, e.getMessage()));
        else if (e instanceof HttpMediaTypeNotSupportedException ||
                e instanceof HttpRequestMethodNotSupportedException)
            response.setError(new ErrorData(405, e.getMessage()));
        else
            response.setError(new ErrorData(500, e.getMessage()));

        return ApiUtil.response(null, response,
                HttpStatus.valueOf(((ErrorData) response.getError()).getCode()));
    }
}
