package com.t3h.utils;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
@Getter
public class ConnectDB {
    @Value("${urlDB}")
    private String urlDB;
    @Value("${dbName}")
    private String dbName;
    @Value("${user}")
    private String user;
    @Value("${pass}")
    private String pass;

    public Connection getConnection() throws SQLException {
        String url = String.format("%s/%s?user=%s&password=%s", urlDB, dbName, user, pass);
        return DriverManager.getConnection(url);
    }
}
