package com.t3h.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.nio.charset.StandardCharsets;

public class ApiUtil {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static ResponseEntity<?> response(String format, Object body, HttpStatus status) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        if ("xml".equals(format)) {
            headers.setContentType(new MediaType(MediaType.APPLICATION_XML, StandardCharsets.UTF_8));
            return new ResponseEntity<>(mapper.writeValueAsString(body), headers, status);
        }
        headers.setContentType(new MediaType(MediaType.APPLICATION_JSON, StandardCharsets.UTF_8));
        return new ResponseEntity<>(mapper.writeValueAsString(body), headers, status);
    }
}
