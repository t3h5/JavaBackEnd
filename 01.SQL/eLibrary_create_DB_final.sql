USE eLibrary
 
CREATE TABLE  users_information (
	user_id		int				NOT NULL AUTO_INCREMENT,
	username	nvarchar(60)	UNIQUE,
	password 	nvarchar(100)	NOT NULL,
	first_name	nvarchar(30)	NOT NULL,
	last_name	nvarchar(30)	NOT NULL,
	email		nvarchar(50)	NOT NULL unique,
	mobile		nvarchar(15)	NOT NULL,
	user_type	nvarchar(10)	NOT NULL,
	create_at	timestamp 		NOT NULL,
	status		nvarchar(10)	NOT NULL ,
	expire_date	date,
	PRIMARY  KEY (user_id)
);

CREATE TABLE  loggings (
	log_id		int				AUTO_INCREMENT,
	action_kind	nvarchar(20)	NOT NULL ,
	user_id		int				NOT NULL,
	action_date timestamp		NOT NULL,
	message		text			NOT NULL ,
	PRIMARY KEY (log_id),
	FOREIGN KEY (user_id) REFERENCES users_information (user_id)	
);

CREATE TABLE  book_cagetory (
	cagetory_id	int 			NOT NULL,
	name		nvarchar(20)	NOT NULL,
	parent_id	int				NOT NULL,
	PRIMARY KEY (cagetory_id) 
);
 

CREATE TABLE  books (
	book_id			int				NOT NULL,
	name 			nvarchar(40)	NOT NULL,
	author			nvarchar(40)	NOT NULL,
	publisher		nvarchar(40)	NOT NULL,
	publis_date 	timestamp		NOT null,
	cagetory_id		int				NOT NULL,
	book_country	nvarchar(30)	NOT NULL,
	facebook		nvarchar(250)	NOT NULL,
	status			nvarchar(15)	NOT NULL, 
	PRIMARY KEY (book_id),
	FOREIGN KEY (cagetory_id) REFERENCES book_cagetory (cagetory_id)
);


CREATE TABLE borrow_books (
	borrow_id		int 			NOT NULL AUTO_INCREMENT,
	book_Id			int				NOT NULL,
	student_Id		int				NOT NULL,
	borrow_date		timestamp		NOT NULL,
	expire_date		timestamp		NOT NULL,
	status			nvarchar(15)	NOT NULL ,
	comment			nvarchar(250)	,
	PRIMARY KEY (borrow_id), 
	FOREIGN KEY (book_id) REFERENCES books (book_id),	
	FOREIGN KEY (student_id) REFERENCES users_information (user_id) 
);

-- 
-- CREATE TABLE stock ( 
-- 	book_id		int		NOT NULL,
-- 	total		int		NOT NULL (CHECK total >= 0),
-- 	remain		int		NOT NULL (CHECK remain >= 0),
-- 	PRIMARY KEY (stock_id)
-- 	FOREIGN KEY (book_id) REFERENCES books (book_id)
-- );

CREATE TABLE stock_in (
	stock_id	int			NOT NULL,
	book_id     int			NOT NULL,
	quantity	int 		NOT NULL CHECK (quantity > 0),
	added_ad	timestamp	NOT NULL,
	user_id		int 		NOT NULL,
	supplyer	nvarchar(60)NOT NULL,
	FOREIGN KEY (book_id) REFERENCES books (book_id),
	FOREIGN KEY (user_id) REFERENCES users_information (user_id) 
); 