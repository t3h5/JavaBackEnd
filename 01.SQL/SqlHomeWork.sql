use classicmodels;
-- 2. liê?t kê offices co? country la? USA
select * from offices o where o.country = 'USA';

-- 3. liê?t kê tâ?t ca? employess, thêm cô?t alias fullName, b??ng firstname + lastname
select *, concat(e.firstName," ",e.lastName) fullName from employees e; 

-- 4. liê?t kê tâ?t ca? employess, thêm cô?t alias offices.city, office.state
select e.*, o.city ,o.state 
from employees e 
	join offices o on e.officeCode  = o.officeCode;

-- 5. liê?t kê offices ko ch??a employee na?o
-- C1:
select o.*
from offices o 
	left join employees e on o.officeCode = e.officeCode 
where e.employeeNumber is null;

-- C2:
select *
from offices o 
where o.officeCode not in (select distinct e.officeCode  from employees e);

-- 6. ?ê?m sô? l??ng employees
select count(e.employeeNumber) from employees e;

-- 7. liê?t kê customers co? nhiê?u nhâ?t 3 payments
select *
from customers c2 
where c2.customerNumber in (
	select c.customerNumber 
	from customers c
		left join payments p ON p.customerNumber  = c.customerNumber 
	group by c.customerNumber
	having count(c.customerNumber) <= 3);

-- 8. liê?t kê 10 customer co? payments s??m nhâ?t
select c.*,p.paymentDate 
from customers c 
	join payments p on p.customerNumber  = c.customerNumber
order by p.paymentDate  asc 
limit 10;

-- 9. liê?t kê ca?c customers co? salesRepEmployeeNumber tô?n ta?i
select *
from customers c 
where c.salesRepEmployeeNumber is not null;

-- 10. liê?t kê 10 customer co? nhiê?u payment nhâ?t, s?? xê?p gia?m dâ?n sô? l???ng payments
select c2.*,c3.paypmentCount
from customers c2 
	join (
		select p.customerNumber, count(p.customerNumber) as paypmentCount
		from payments p
		group by p.customerNumber
		order by count(p.checkNumber) desc
		limit 10) c3 on c3.customerNumber = c2.customerNumber ;
	
-- 11. ti?m tâ?t ca? orders trong tha?ng 11/2003
select *
from orders o 
where month (o.orderDate) = 11 and year(o.orderDate) = 2003;

-- 12. thêm 2 employee
INSERT INTO employees(employeeNumber, lastName, firstName, extension, email, officeCode, reportsTo, jobTitle)
VALUES(1800, 'Nguyen Huu', 'Phuong', 'abc', 'xyz.gmail.com', '1', NULL, 'Java'),
		(1801, 'Nguyen Van', 'An', 'mno', 'mno.gmail.com', '2', NULL, 'C#');
	
-- 13. thêm 2 office
-- t??ng t? 12

-- 14. s??a addressLine2 = '31 street Red'  cu?a employee co? id 172
update customers  
set addressLine2 = '31 street Red'
where customerNumber  = 172;

-- 15. update tâ?t ca? customer co? addressLine la? Null b??ng addressLine1
update customers 
set addressLine2 = addressLine1 
where addressLine2 is null;

-- 16. update tâ?t ca? customer co? state la? Null b??ng 3 ki? t?? dâ?u cu?a city, viê?t hoa ch?? ca?i ?â?u
update customers 
set state = concat(substr(city,1,1),substr(city,2,2)) 
where state is null;

-- 17. liê?t kê 10 customer co? nhiê?u orders nhâ?t
select ctm.*,ctm2.orderCount
from customers ctm
	inner join (
		select o.customerNumber, count(o.orderNumber) as orderCount
		from orders o
		group by o.customerNumber
		order by count(o.orderNumber) desc
		limit 10) ctm2 on ctm2.customerNumber = ctm.customerNumber;
	
-- 18. liệt kê tất cả orderdetails của từng orders
select o2.orderNumber , o2.orderDate , o.*
from orderdetails o 
	join orders o2 on o.orderNumber  = o2.orderNumber ;

-- 19. liệt kê tất cả orderdetails, order có productCode bắt đầu là S10
select  o2.*, o.*
from orderdetails o 
	join orders o2 on o.orderNumber  = o2.orderNumber
where o.productCode like 'S10%';

-- 20. liệt kê 11 product có ít orderdetails nhâṭ
select p.*, p2.totalQuantityOrderred
from products p 
	join (
		select o.productCode , sum(quantityOrdered) totalQuantityOrderred
		from orderdetails o 
		group by o.productCode 
		order by sum(quantityOrdered) 
		limit 11
	) as p2 on p.productCode  = p2.productCode
order by p2.totalQuantityOrderred;

-- 21. liệt kê thông tin productlines, products, orderdetails của từng order
select o.*, p.*
from orderdetails o 
	join products p on o.productCode  = p.productCode;

-- 22. thêm 3 products

-- 23. xóa customers không có bất kì orders nào
delete from customers where customerNumber not in (select customerNumber from orders );

-- 24. liệt kê orders, orderdetails theo từng customers
select o.customerNumber , o.*, o2.*
from orders o 
	join orderdetails o2 on o.orderNumber  = o2.orderNumber;

-- 25. liệt kê customers tại USA có số lượng payment nhiều nhất
select c.*,count(c.customerNumber)
from customers c 
	join payments p on c.customerNumber  = p.customerNumber 
where c.country  = 'USA'
group by c.customerNumber 
order by count(c.customerNumber) desc;

-- 26. liệt kê 3 customers có lượng amout it́ nhất
select c.*,sum(p.amount)
from customers c 
	join payments p on c.customerNumber  = p.customerNumber 
where c.country  = 'USA'
group by c.customerNumber 
order by sum(p.amount)
limit 3;

--  27. liệt kê 5 products có số lượng payment nhiều nhất
select *, count(o.orderNumber)
from orderdetails o 
group by o.productCode 
order by count(*) desc
limit 5;

-- 28. liệt kê orders được thanh toán trong tháng 4-2003 & tháng 12-2003
select * 
from orders o2 
where o2.customerNumber in (
	select p.customerNumber 
	from payments p
	where month (p.paymentDate) = 4 and year (p.paymentDate) = 2003
	group by p.customerNumber 
);

-- 29. liệt kê 1 office có số lượng employee chăm sóc nhiều customers nhất
select o.*
from offices o
	join (
		select e.officeCode, sum(e2.totalCustomer)
		from employees e 
			join (
				select c.salesRepEmployeeNumber , count(c.customerNumber) totalCustomer
				from customers c 
				group by salesRepEmployeeNumber 
				order by count(c.customerNumber) desc
			) e2 on e.employeeNumber  = e2.salesRepEmployeeNumber
		group by e.officeCode 
		order by sum(e2.totalCustomer) desc
		limit 1
	) as o2 on o.officeCode = o2.officeCode;

-- 30. liệt kê 2 customer có giới hạn tín dụng < 20000
select *
from customers c 
where c.creditLimit < 20000
limit 2;

-- 31. liệt kê 2 products tồn hàng nhiều nhất
select *
from products p 
order by p.quantityInStock desc 
limit 2;

-- 32. liêt kê 10 products có giá thấp nhất & được nhiều KH chọn nhất
select p.*, p2.soKhangMua, p2.tongLanChonMua
from products p 
	join (
		select tmp.productCode, count(*) soKhangMua, sum(tmp.lanChonMua) as tongLanChonMua
		from(
			select c.customerNumber , od.productCode, count(*) lanChonMua 
			from customers c 
				join orders o on c.customerNumber  = o.customerNumber  
				join orderdetails od on o.orderNumber  = od.orderNumber 
			group by c.customerNumber , od.productCode 
		) tmp
		group by tmp.productCode
		order by count(*) desc, sum(tmp.lanChonMua) desc
		limit 10
	) p2 on p.productCode  = p2.productCode;

-- 33. liệt kê 5 sản phẩm bị Cancelled nhất
select p.*, p2.soLanHuy
from products p 
	join (
		select o2.productCode, count(*) soLanHuy
		from orders o
			join orderdetails o2 on o.orderNumber  = o2.orderNumber 
		where o.status = 'Cancelled'
		group by o2.productCode 
		order by count(*) desc
		limit 5
	) p2 on p.productCode  = p2.productCode
order by p2.soLanHuy desc;

-- 34. liệt kê 5 sản phâm được giao hàng sớm nhất trong năm 2004
select *, (o.shippedDate -o.orderDate) timeGiaoHang, (o.requiredDate - o.shippedDate) timeConLai
from orders o 
where year (o.shippedDate) = 2004
order by timeGiaoHang, timeConLai desc
limit 5;













	
	


